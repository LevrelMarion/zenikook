package com.marion.zenikook.service;

import com.marion.zenikook.controllers.representation.recipe.NewRecipeRepresenation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentation;
import com.marion.zenikook.controllers.representation.steps.StepsRepresentation;
import com.marion.zenikook.domain.Recipe;
import com.marion.zenikook.domain.Steps;
import com.marion.zenikook.repositories.RecipeRepository;
import com.marion.zenikook.repositories.StepsRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RecipeService {

        private RecipeRepository recipeRepository;
        private IdGenerator idGenerator;

    public RecipeService(RecipeRepository recipeRepository, IdGenerator idGenerator) {
        this.recipeRepository = recipeRepository;
        this.idGenerator = idGenerator;
    }

    //tenter un foreach pour empecher le cast // itérer sur chaque ligne du tableau
    public List<Recipe> getAllQRecipe() {
        return (List<Recipe>) this.recipeRepository.findAll();
    }

    public Optional<Recipe> getOneRecipe(String id) {
        return this.recipeRepository.findById(id);
    }

    public void deleteOneRecipe(String id) {
       this.recipeRepository.deleteById(id);
    }

    public Recipe createOneRecipe (NewRecipeRepresenation body) {
        List<Steps> listNewSteps = new ArrayList<>();
        for (Steps s : body.getStepsList()) {
            Steps newSteps = new Steps(this.idGenerator.generateNewId(), s.getOrderStep(), s.getDescription());
            listNewSteps.add(newSteps);
        }
        Recipe recipe = new Recipe(this.idGenerator.generateNewId(), body.getName(), body.getRecipeContent(), listNewSteps, body.getImageRecipe());
        return this.recipeRepository.save(recipe);
    }

    public Recipe modifyOneRecipe (String id, RecipeRepresentation body) {
        // A FAIRE : ne pas sortir de l'optional avec
        Recipe recipeToModify = recipeRepository.findById(id).get();
        Recipe recipeInBase = new Recipe(id, body.getName(), body.getRecipeContent(), body.getStepsList(), body.getImageRecipe());
        String newRecipeContent = "";
        String newName = "";
        String newImage = "";
        List<Steps> newStepList;
        if (recipeInBase.getName().equals("")) {
            newName = recipeToModify.getName();
        } else {
            newName = recipeInBase.getName();
        }
        if (recipeInBase.getRecipeContent().equals("")) {
            newRecipeContent = recipeToModify.getRecipeContent();
        } else {
            newRecipeContent = recipeInBase.getRecipeContent();
        }
        if (recipeInBase.getStepsList().isEmpty()) {
            newStepList = recipeToModify.getStepsList();
        } else {
            newStepList = recipeInBase.getStepsList();
        }
        if (recipeInBase.getImageRecipe().equals("")) {
            newImage = recipeToModify.getImageRecipe();
        } else {
            newImage = recipeInBase.getImageRecipe();
        }

        Recipe newRecipe = new Recipe(id,newName, newRecipeContent, newStepList, newImage);
        return this.recipeRepository.save(newRecipe);

    }
}

/*
    Optional<Recipe> toModify = this.recipeRepository.findById(id);
    if (toModify.isPresent()) {
        if (toModify.get().getName() != body.getName() && body.getName() != null) {
            toModify.get().setName(body.getName());
        }
        if (toModify.get().getRecipeContent() != body.getRecipe_content() && body.getRecipe_content() != null) {
            toModify.get().setRecipeContent(body.getRecipe_content());
        }
    }
    this.recipeRepository.save(toModify.get());
}
 */