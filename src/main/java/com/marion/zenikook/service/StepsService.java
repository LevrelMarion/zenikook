package com.marion.zenikook.service;

import com.marion.zenikook.controllers.representation.steps.StepsRepresentation;
import com.marion.zenikook.domain.Steps;
import com.marion.zenikook.repositories.StepsRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class StepsService {

    private StepsRepository stepsRepository;
    private IdGenerator idGenerator;

    public StepsService(StepsRepository stepsRepository, IdGenerator idGenerator) {
        this.stepsRepository = stepsRepository;
        this.idGenerator = idGenerator;
    }

    public List<Steps> getAllSteps() {
        return (List<Steps>) this.stepsRepository.findAll();
    }

    public Optional<Steps> getOneStep(String id) {
        return this.stepsRepository.findById(id);
    }

    public void deleteOneStep(String id) {
        this.stepsRepository.deleteById(id);
    }

    public Steps createOneStep (StepsRepresentation body) {
        Steps steps =new Steps(this.idGenerator.generateNewId(), body.getOrderStep(), body.getStepDescription());
        return this.stepsRepository.save(steps);
    }

    public Steps modifyOneStep (String id, StepsRepresentation body) {
        // A FAIRE : ne pas sortir de l'optional avec
        Steps stepToModify = stepsRepository.findById(id).get();
        Steps stepsInBase = new Steps(id,body.getOrderStep(), body.getStepDescription());
        int newStepOrder;
        String newStepDescription = "";
        if (stepsInBase.getDescription().equals("")) {
            newStepDescription = stepToModify.getDescription();
        } else {
            newStepDescription = stepsInBase.getDescription();
        }
        if (stepsInBase.getOrderStep() == 0) {
            newStepOrder = stepToModify.getOrderStep();
        } else {
            newStepOrder = stepsInBase.getOrderStep();
        }

        Steps newStep = new Steps(id, newStepOrder, newStepDescription);
        return this.stepsRepository.save(newStep);

    }
}
