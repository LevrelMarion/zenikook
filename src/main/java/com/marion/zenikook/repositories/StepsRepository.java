package com.marion.zenikook.repositories;

import com.marion.zenikook.domain.Steps;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StepsRepository extends CrudRepository<Steps, String> {
}
