package com.marion.zenikook.controllers.representation.recipe;
import com.fasterxml.jackson.annotation.JsonInclude;

import com.marion.zenikook.domain.Steps;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewRecipeRepresenation {
    private String name;
    private String recipeContent;
    private List<Steps> stepsList;
    private String imageRecipe;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }


    public List<Steps> getStepsList() {
        return stepsList;
    }

    /* public List<Steps> getStepsListToSteps() {
        //IdGenerator idGenerator = new IdGenerator();
        //StepsRepresentation stepsRepresentation = new StepsRepresentation();
        List<Steps> listSteps = stepsList.stream().map(Steps::toString)
        return listSteps;
    }*/

    public void setStepsList(List<Steps> stepsList) {
        this.stepsList = stepsList;
    }

    public String getImageRecipe() {
        return imageRecipe;
    }

    public void setImageRecipe(String imageRecipe) {
        this.imageRecipe = imageRecipe;
    }
}




/*
    public void addStepsList(Steps step) {
        this.stepsList.add(step);
    }

            List<String> listString = stepsList.stream()
                .map(x -> x.getDescription())
                .collect(Collectors.toList());
 */