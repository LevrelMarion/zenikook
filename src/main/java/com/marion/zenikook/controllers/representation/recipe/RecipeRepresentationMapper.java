package com.marion.zenikook.controllers.representation.recipe;

import com.marion.zenikook.domain.Recipe;
import org.springframework.stereotype.Component;

@Component
public class RecipeRepresentationMapper {
    public RecipeRepresentation mapToDisplayableRecipe(Recipe recipe) {
        RecipeRepresentation result = new RecipeRepresentation();
        result.setId(recipe.getId());
        result.setName(recipe.getName());
        result.setRecipeContent(recipe.getRecipeContent());
        result.setStepsList(recipe.getStepsList());
        result.setImageRecipe(recipe.getImageRecipe());
        return result;
    }

}
