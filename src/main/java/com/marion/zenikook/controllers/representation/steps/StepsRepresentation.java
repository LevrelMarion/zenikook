package com.marion.zenikook.controllers.representation.steps;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StepsRepresentation {


    private int orderStep;
    private String stepDescription;

    public int getOrderStep() { return orderStep; }

    public void setOrderStep(int orderStep) {
        this.orderStep = orderStep;
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }
}
