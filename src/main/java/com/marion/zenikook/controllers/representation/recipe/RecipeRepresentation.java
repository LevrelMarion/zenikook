package com.marion.zenikook.controllers.representation.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.marion.zenikook.domain.Steps;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecipeRepresentation {
    private String id;
    private String name;
    private String recipeContent;
    private List<Steps> stepsList;
    private String imageRecipe;


    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipeContent() {
        return this.recipeContent;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public List<Steps> getStepsList() {
        return stepsList;
    }

    public void setStepsList(List<Steps> stepsList) {
        this.stepsList = stepsList;
    }

    public String getImageRecipe() {
        return imageRecipe;
    }

    public void setImageRecipe(String imageRecipe) {
        this.imageRecipe = imageRecipe;
    }
}
