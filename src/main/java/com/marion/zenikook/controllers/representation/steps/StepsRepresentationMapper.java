package com.marion.zenikook.controllers.representation.steps;

import com.marion.zenikook.controllers.representation.ingredient.IngredientRepresentation;
import com.marion.zenikook.domain.Ingredient;
import com.marion.zenikook.domain.Steps;
import org.springframework.stereotype.Component;

@Component
public class StepsRepresentationMapper {
    public StepsRepresentation mapToDisplayableSteps(Steps steps) {
        StepsRepresentation result = new StepsRepresentation();
        result.setStepDescription(steps.getDescription());
        result.setOrderStep(steps.getOrderStep());
        return result;
    }
}
