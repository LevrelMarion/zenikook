package com.marion.zenikook.controllers;

import com.marion.zenikook.controllers.representation.recipe.NewRecipeRepresenation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentationMapper;
import com.marion.zenikook.controllers.representation.steps.StepsRepresentation;
import com.marion.zenikook.controllers.representation.steps.StepsRepresentationMapper;
import com.marion.zenikook.domain.Recipe;
import com.marion.zenikook.domain.Steps;
import com.marion.zenikook.service.RecipeService;
import com.marion.zenikook.service.StepsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/steps")
public class StepsController {

    private StepsService stepsService;
    private StepsRepresentationMapper stepsRepresentationMapper;

    @Autowired
    public StepsController(StepsService stepsService, StepsRepresentationMapper stepsRepresentationMapper) {
        this.stepsService = stepsService;
        this.stepsRepresentationMapper = stepsRepresentationMapper;
    }

    @GetMapping
    List<StepsRepresentation> getAllSteps() {
        return this.stepsService.getAllSteps().stream()
                .map(this.stepsRepresentationMapper::mapToDisplayableSteps)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<StepsRepresentation> getOneStep(@PathVariable("id") String id) {
        Optional<Steps> steps = this.stepsService.getOneStep(id);
        return steps
                .map(this.stepsRepresentationMapper::mapToDisplayableSteps)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StepsRepresentation> deleteOneStep(@PathVariable("id") String id) {
       this.stepsService.deleteOneStep(id);
       return ResponseEntity.noContent().build();
    }

    @PostMapping
    public StepsRepresentation createStep(@RequestBody StepsRepresentation body) {
        return stepsRepresentationMapper.mapToDisplayableSteps(this.stepsService.createOneStep(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StepsRepresentation> modifyStep(@PathVariable ("id") String id ,
                                                  @RequestBody StepsRepresentation body) {
        this.stepsService.modifyOneStep(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
