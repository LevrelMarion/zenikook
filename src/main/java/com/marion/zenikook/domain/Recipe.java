package com.marion.zenikook.domain;

import javax.persistence.*;

import java.util.List;

@Entity
@Table(name="recipes")
public class Recipe {

    @Id
    private String id;
    private String name;
    private String recipeContent;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="id_recipe", referencedColumnName="id")
    private List<Steps> stepsList;
    private String imageRecipe;

    public Recipe(String id, String name, String recipeContent, List<Steps> stepsList, String image) {
        this.id = id;
        this.name = name;
        this.recipeContent = recipeContent;
        this.stepsList = stepsList;
        this.imageRecipe = image;
    }

    protected Recipe() {}

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public List<Steps> getStepsList() {
        return stepsList;
    }

    public void setStepsList(List<Steps> stepsList) {
        this.stepsList = stepsList;
    }

    public String getImageRecipe() {
        return imageRecipe;
    }

    public void setImageRecipe(String imageRecipe) {
        this.imageRecipe = imageRecipe;
    }
}
