package com.marion.zenikook.domain;

import javax.persistence.*;

@Entity
@Table
public class Steps {

    @Id
    private String id;
    private int orderStep;
    private String description;

    public Steps(String id, int orderStep, String description) {
        this.description = description;
        this.id = id;
        this.orderStep = orderStep;
    }

    protected Steps () {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrderStep() {
        return orderStep;
    }

    public void setOrderStep(int orderStep) {
        this.orderStep = orderStep;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
