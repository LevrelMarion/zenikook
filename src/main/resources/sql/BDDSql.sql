create table recipes
(
id text primary key,
name text not null,
recipe_content text not null
);

create table steps
(
id text PRIMARY KEY,
order_step int NOT null,
id_recipe text REFERENCES recipes (id),
description text not null
);

create table ingredient
(
id text primary key,
name text not null
);

create table composition
(
   id_ingredient text references ingredients (id),
   id_recipe text references recipes (id),
   unit text not null,
   quantity int not null,
   PRIMARY KEY (id_ingredient, id_recipe)
);

INSERT INTO steps (id, order_step, id_recipe, description)
VALUES
('1','1','1', 'mettre la galette dans la poëlle'),
('2','2','1', 'mettre le jambon dans la galette'),
('3','1','2', 'mettre la creamCheese dans un saladier'),
('4','2','2', 'ajouter les oeufs');


SELECT steps.description, recipe.name
FROM steps
JOIN recipes r on steps.id_recipe = r.id



insert into recipes (id, name, recipe_content)
values ('1', 'galettes complètes', 'galette, jambon, oeuf, fromage'),
('2','Lemon cheesecake','creamCheese, speculos, oeuf, lait, sucre'),
('3','oeuf à la mexicaine','oeuf, tomate, piments verts, oignon, sel');

insert into ingredient (id, name)
values
('1', 'galette de blé noir'),
('2', 'jambon'),
('3', 'oeuf'),
('4', 'fromage')
('5', 'creamCheese'),
('6', 'speculos'),
('7', 'lait'),
('8', 'sucre'),
('9', 'tomate'),
('10', 'piments verts'),
('11', 'oignon'),
('12', 'sel');

insert into recipe (id, name, id_ingredient)
values
('1', 'galette complète', SELECT ),
('2', 'Lemon cheesecake', 5 6 7 8),
('3', 'oeuf à la mexicaine', 9 10 11 12)

insert into composition (id_ingredient, id_recipe, unit, quantity)
values
('1', '1', 'unité', 1),
('2', '1', 'grammes', 30),
('3', '1', 'unité', 1),
('4', '1', 'grammes', 10),
('5', '2', 'grammes', 50),
('6', '2', 'unité', 5),
('7', '2', 'centilitres', 20),
('8', '2', 'grammes', 30),
('3', '2', 'unité', 2),
('3', '3', 'unité', 2),
('9', '3', 'unité', 2),
('10', '3', 'unité', 1),
('11', '3', 'unité', 1),
('12', '3', 'pincée', 1);


/* create table composition
(
id_ingredient text NOT NULL ,
id_recipe text NOT NULL ,
unit text not null,
quantity int not null
PRIMARY KEY( id_recipe, id_ingredient ),
CONSTRAINT fk_recette_composition
FOREIGN KEY id_recette REFERENCES recette ( id_recette ),
)
( id_recette [pk][fk], id_ingredient [pk][fk], quantite, ordre )
composition (
    id_recipe TEXT UNSIGNED NOT NULL,
    id_ingredient TEXT UNSIGNED NOT NULL,
    PRIMARY KEY( id_recette, id_ingredient, ordre ),
    KEY ( id_recette ),
    CONSTRAINT fk_recette_composition
        FOREIGN KEY id_recette
        REFERENCES recette ( id_recette ),
    KEY ( id_ingredient ),
    CONSTRAINT fk_ingredient_composition
        FOREIGN KEY id_recette
        REFERENCES ingredient ( id_ingredient )

DELETE FROM recipes
WHERE recipes.id = '4d383fac-0941-4f36-967a-fedbf07fefcb';

ALTER TABLE measures
ADD PRIMARY KEY (id_ingredient, id_recipe)
alter table measures rename to composition;

select ingredient.name, composition.quantity
FROM composition
JOIN ingredient
ON composition.id_ingredient = ingredient.id;

select recipe.name, composition.quantity
FROM composition
JOIN ingredient
ON composition.id_ingredient = ingredient.id;


create table recipe
(
id text primary key,
name text not null,
id_ingredient text references ingredients (id)
);
)*/
