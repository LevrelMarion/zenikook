FROM openjdk:13.0-slim
# image de base de laquelle on part
COPY target/*.jar /app.jar
# ajout dans l'image de base
# je prends dans le contexte (dossier courrant tout ce qu'on est en train de faire / le travail sur le back) de l'image et le le met dans la racine de l'image
EXPOSE 8081
CMD java -java /app.jar
#pas besoin de spring car ici c'est pour builder